//
//  GameScene.swift
//  MultiPlayer Game
//
//  Created by Satinder pal Singh on 2019-12-02.
//  Copyright © 2019 Satinder pal Singh. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene {
    
    private var label : SKLabelNode?
    private var spinnyNode : SKShapeNode?
    
    let background1 = SKSpriteNode(imageNamed: "bg")
    let background2 = SKSpriteNode(imageNamed: "bg")
    let cat = SKSpriteNode(imageNamed: "Run1")
    let dog = SKSpriteNode(imageNamed: "dogRun1")
    var counterTimer = Timer()
       var counter = 0
       var boxArray:[SKSpriteNode] = [SKSpriteNode]()
//
    func backgroundPlacement(){
        background1.position = CGPoint(x: frame.size.width / 2, y:frame.size.height / 2)
        background1.size = CGSize(width: frame.width, height: frame.height)
        background1.anchorPoint = CGPoint.zero
        background1.position = CGPoint(x: 0, y: 80)
        background1.zPosition = -15
        self.addChild(background1)

        background2.size = CGSize(width: frame.width, height: frame.height)
        background2.anchorPoint = CGPoint.zero
        background2.position = CGPoint(x: background1.size.width - 1,y: 80)
        background2.zPosition = -15
        self.addChild(background2)
    }
    
    func moveBackgroundLoop(){
        background1.position = CGPoint(x: background1.position.x-2, y: background1.position.y)
        background2.position = CGPoint(x: background2.position.x-2, y: background2.position.y)
        
        for (index, box) in self.boxArray.enumerated() {
                   box.position.x = box.position.x-2
                   
               }
        if(background1.position.x < -background1.size.width)
        {
            background1.position = CGPoint(x: background1.size.width-4,y: 80)
        }
        if(background2.position.x < -background2.size.width)
        {
            background2.position = CGPoint(x: background1.size.width-4,y: 80)
        }
    }
    
    override func didMove(to view: SKView) {
        
        // Get label node from scene and store it for use later
        
        
        backgroundPlacement()
        cat.position = CGPoint(x:self.size.width*0.25, y:240)
        addChild(cat)
        catAnimation()
        dog.position = CGPoint(x:self.size.width*0.22, y:290)
        addChild(dog)
        dogAnimation()
        startCounter()
        
    }
     func startCounter(){
            
               counterTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(incrementCounter), userInfo: nil, repeats: true)
               
              
           }
           @objc func incrementCounter(){
            self.counter = self.counter + 1
            
            if(self.counter % 5 == 0){
               
            let randomInt = Int.random(in: 1...3)
            print("random number \(randomInt)")
                
                      if(randomInt == 1){
                         let box = SKSpriteNode(imageNamed: "box")
                       box.position = CGPoint(x: size.width+50, y:160)
                                                   box.size = CGSize(width: 160, height: 160)
                                                   box.anchorPoint = CGPoint.zero

                                                   box.zPosition = -13
                                                   self.addChild(box)
                                                  self.boxArray.append(box)
                               print(boxArray.count)
                          
                      }
                      if(randomInt == 2){
                         let box = SKSpriteNode(imageNamed: "box")
                          box.position = CGPoint(x: size.width+50, y:160)
                                                      box.size = CGSize(width: 160, height: 160)
                                                      box.anchorPoint = CGPoint.zero

                                                      box.zPosition = -13
                                                      self.addChild(box)
                                                     self.boxArray.append(box)
                                  print(boxArray.count)
                        
                         let box1 = SKSpriteNode(imageNamed: "box")
                        box1.position = CGPoint(x: size.width+50, y:210)
                                                    box1.size = CGSize(width: 160, height: 160)
                                                    box1.anchorPoint = CGPoint.zero

                                                    box1.zPosition = -13
                                                    self.addChild(box1)
                                                   self.boxArray.append(box1)
                                print(boxArray.count)
                      }
                      if(randomInt == 3){
                         let box = SKSpriteNode(imageNamed: "box")
                          box.position = CGPoint(x: size.width+50, y:160)
                                                      box.size = CGSize(width: 160, height: 160)
                                                      box.anchorPoint = CGPoint.zero

                                                      box.zPosition = -13
                                                      self.addChild(box)
                                                     self.boxArray.append(box)
                                  print(boxArray.count)
                        
                        let box1 = SKSpriteNode(imageNamed: "box")
                        box1.position = CGPoint(x: size.width+50, y:210)
                                                    box1.size = CGSize(width: 160, height: 160)
                                                    box1.anchorPoint = CGPoint.zero

                                                    box1.zPosition = -13
                                                    self.addChild(box1)
                                                   self.boxArray.append(box1)
                                print(boxArray.count)
                        
                        let box2 = SKSpriteNode(imageNamed: "box")
                        box2.position = CGPoint(x: size.width+50, y:260)
                                                    box2.size = CGSize(width: 160, height: 160)
                                                    box2.anchorPoint = CGPoint.zero

                                                    box2.zPosition = -13
                                                    self.addChild(box2)
                                                   self.boxArray.append(box2)
                                print(boxArray.count)
                      }
            }
            
        }
    //    func makeBoxes(){
    //        box.position = CGPoint(x: background2.position.x+50, y:background2.position.y+50)
    //        box.size = CGSize(width: 100, height: 100)
    //        box.anchorPoint = CGPoint.zero
    //
    //        background1.zPosition = -16
    //        self.addChild(box)
    //
    //
    //
    //    }
    func catAnimation(){

        let image1 = SKTexture(imageNamed: "Run1")
        let image2 = SKTexture(imageNamed: "Run2")
        let image3 = SKTexture(imageNamed: "Run3")
        let image4 = SKTexture(imageNamed: "Run4")
        let image5 = SKTexture(imageNamed: "Run5")
        let image6 = SKTexture(imageNamed: "Run6")
        let image7 = SKTexture(imageNamed: "Run7")
        let image8 = SKTexture(imageNamed: "Run8")
        let punchTextures = [image1,image2,image3,image4,image5,image6,image7,image8]
        let punchAnimation = SKAction.animate(
                       with: punchTextures,
                       timePerFrame: 0.1)
        self.cat.run(punchAnimation)
        let catAnimationForeever = SKAction.repeatForever(punchAnimation)
        self.cat.run(catAnimationForeever)
        cat.zPosition = 1
        
    }
    func dogAnimation(){
        let image1 = SKTexture(imageNamed: "dogRun1")
         let image2 = SKTexture(imageNamed: "dogRun2")
         let image3 = SKTexture(imageNamed: "dogRun3")
         let image4 = SKTexture(imageNamed: "dogRun4")
         let image5 = SKTexture(imageNamed: "dogRun5")
         let image6 = SKTexture(imageNamed: "dogRun6")
         let image7 = SKTexture(imageNamed: "dogRun7")
         let image8 = SKTexture(imageNamed: "dogRun8")
         let punchTextures = [image1,image2,image3,image4,image5,image6,image7,image8]
         let punchAnimation = SKAction.animate(
                        with: punchTextures,
                        timePerFrame: 0.1)
         self.dog.run(punchAnimation)
         let dogAnimationForeever = SKAction.repeatForever(punchAnimation)
         self.dog.run(dogAnimationForeever)
        
    }
    func touchDown(atPoint pos : CGPoint) {
        if let n = self.spinnyNode?.copy() as! SKShapeNode? {
            n.position = pos
            n.strokeColor = SKColor.green
            self.addChild(n)
        }
    }
    
    func touchMoved(toPoint pos : CGPoint) {
        if let n = self.spinnyNode?.copy() as! SKShapeNode? {
            n.position = pos
            n.strokeColor = SKColor.blue
            self.addChild(n)
        }
    }
    
    func touchUp(atPoint pos : CGPoint) {
        if let n = self.spinnyNode?.copy() as! SKShapeNode? {
            n.position = pos
            n.strokeColor = SKColor.red
            self.addChild(n)
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let label = self.label {
            label.run(SKAction.init(named: "Pulse")!, withKey: "fadeInOut")
        }
        
        for t in touches { self.touchDown(atPoint: t.location(in: self)) }
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        for t in touches { self.touchMoved(toPoint: t.location(in: self)) }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        for t in touches { self.touchUp(atPoint: t.location(in: self)) }
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        for t in touches { self.touchUp(atPoint: t.location(in: self)) }
    }
    
    
    override func update(_ currentTime: TimeInterval) {
        // Called before each frame is rendered
        
        
        moveBackgroundLoop()
    }
    


}
